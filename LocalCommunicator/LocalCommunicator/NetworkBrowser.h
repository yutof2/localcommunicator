//
//  NetworkBrowser.h
//  SharedBoard
//
//  Created by Yuto Fujikawa on 4/28/13.
//  Copyright (c) 2013 Yuto Fujikawa. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NetworkBrowserDelegate;

@interface NetworkBrowser : NSObject<NSNetServiceDelegate, NSNetServiceBrowserDelegate>
{
    NSMutableArray *_serviceArray;
}
@property (nonatomic, copy) NSString*myServiceName;
@property (nonatomic, retain, readwrite) NSNetService *myNetService;
@property (nonatomic, retain, readwrite) NSNetService *currentNetService;
@property (nonatomic, retain, readwrite) NSMutableArray *serviceArray;
@property (nonatomic, retain, readwrite) NSNetServiceBrowser *netServiceBrowser;
@property (assign, nonatomic) id<NetworkBrowserDelegate>delegate;

-(id)initWithMyServiceName:(NSString*)myServicename;
-(void)searchForServicesOfType:(NSString*)typeString inDomain:(NSString *)domainString;
-(BOOL)resolveServiceWithName:(NSString*)serviceName;
-(void)stopCurrentResolve;

//-(void)search
@end


@protocol NetworkBrowserDelegate <NSObject>
@optional
-(void)didFindNewService:(NetworkBrowser*)browserObj;
-(void)didResolveService:(NetworkBrowser*)browserObj;
-(void)didRemoveConnectedService:(NSString*)serviceName;

@end