//
//  LocalCommunicator.m
//  SharedBoard
//
//  Created by Yuto Fujikawa on 5/11/13.
//  Copyright (c) 2013 Yuto Fujikawa. All rights reserved.
//

#import "LocalCommunicator.h"

@interface LocalCommunicator ()
//-(void)initialize;
-(void)setupServer;
-(void)openStreams;
-(void)setupServerBrwoserWithName:(NSString*)name;
-(void)checkWaitingList;

@end

@implementation LocalCommunicator
@synthesize isReady = _isReady;
@synthesize receivedDictionary = _receivedDictionary;

//int sentDict=0;
//int receivedDict =0;

-(void)dealloc
{
    [_inStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
	[_inStream release];
    
	[_outStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
	[_outStream release];
        
    [_server release];
    _server = nil;
    
    [_dictionaryArray release];
    
    if(_waitingListChecker)
    {
        [_waitingListChecker invalidate];
        _waitingListChecker = NULL;
    }
   [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        self.isServiceAvailable = NO;
        _didAcceptConnection = NO;
        
        [self setupServer];
    }
    return self;
}

-(void)setupServer
{
    LOG(@"%s",(char*)_cmd);
    _isReady = NO;
    
    [_inStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[_inStream release];
	_inStream = nil;
	_inReady = NO;
    
	[_outStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[_outStream release];
	_outStream = nil;
	_outReady = NO;
	
	_server = [TCPServer new];
	[_server setDelegate:self];
	NSError *error = nil;
	if(_server == nil || ![_server start:&error]) {
		if (error == nil) {
			LOG(@"Failed creating server: Server instance is nil");
		} else {
            LOG(@"Failed creating server: %@", error);
		}
		return;
	}
	
	//Start advertising to clients, passing nil for the name to tell Bonjour to pick use default name
	if(![_server enableBonjourWithDomain:@"local" applicationProtocol:[TCPServer bonjourTypeFromIdentifier:ServiceName] name:nil]) {
		LOG(@"Failed advertising server");
		return;
	}
    
    _dictionaryArray = [[NSMutableArray alloc]init] ;
    _waitingListChecker = [NSTimer scheduledTimerWithTimeInterval:0.35f target:self selector:@selector(checkWaitingList) userInfo:nil repeats:YES];
    

    
}


-(void)openStreams
{
    LOG(@"%s",(char*)_cmd);
    _didAcceptConnection = YES;
    self.isServiceAvailable = YES;
    
    _inStream.delegate = self;
	[_inStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[_inStream open];
	_outStream.delegate = self;
	[_outStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[_outStream open];
}

-(void)setupServerBrwoserWithName:(NSString*)serviceName
{
    LOG(@"%s",(char*)_cmd);
    LOG(@"service name %@", serviceName);    
    _networkBrowser = [[NetworkBrowser alloc]initWithMyServiceName:serviceName] ;
    _networkBrowser.delegate = self;
    [_networkBrowser searchForServicesOfType:[TCPServer bonjourTypeFromIdentifier:ServiceName] inDomain:@"local"];
}
#pragma mark - Class methods
-(void)sendDictionary:(NSDictionary*)dict
{
    
    LOG(@"%s",(char*)_cmd);
    @synchronized(self){
    
    //if(self.isServiceAvailable == NO)
    //    return;
        
    static BOOL started = NO;
        
    if (_outReady && _outStream && [_outStream hasSpaceAvailable])
    {
        //sentDict++;
        //LOG(@"sent dictionary is %d", sentDict);
        _outReady = NO;
        started = YES;
        
        NSMutableData *writeData_ = [[NSMutableData alloc] init];
        [writeData_ setData:[NSData dataWithBytes:NULL length:0]];
        
        NSKeyedArchiver *archiver_ = [[NSKeyedArchiver alloc] initForWritingWithMutableData:writeData_];
        [archiver_ encodeObject:dict forKey:EncodingKey];
        [archiver_ finishEncoding];
        int byteIndex_ = 0;
    
    
        while(byteIndex_ < [writeData_ length])
        {
            uint8_t *readBytes = (uint8_t *)[writeData_ mutableBytes];
            readBytes += byteIndex_; // instance variable to move pointer
            int data_len = [writeData_ length];
            unsigned int len = ((data_len - byteIndex_ >= 1024) ? 1024 : (data_len-byteIndex_));
            uint8_t buf[len];
            (void)memcpy(buf, readBytes, len);
        
            len = [_outStream write:(const uint8_t *)buf maxLength:len];
            if(len == -1)
            {
                byteIndex_ = [writeData_ length];
                LOG(@"Failed sending data to peer");
            }
            else
            {
                byteIndex_ += len;
            }
        
        }
    
        [archiver_ release];
        [writeData_ release];
        started = NO;
        

    }
    else
    {
     
        //[_dictionaryArray addObject:dict];
        
    }
        
    }
}

-(void)addToDictionaryArray:(NSDictionary*)dict
{
    //if(self.isServiceAvailable == NO)
    //    return;
    
    [_dictionaryArray addObject:dict];
}
-(void)resolveService:(NSString*)serviceName
{
    if(_didAcceptConnection==NO)
        [_networkBrowser resolveServiceWithName:serviceName];
}

#pragma mark - TCPServerDelegate
- (void) serverDidEnableBonjour:(TCPServer *)server withName:(NSString *)string
{
	LOG(@"%s", (char*)_cmd);
	//[self presentPicker:string];
    //[self setUpServerBrwoser];
    [self setupServerBrwoserWithName:string];
    
}

- (void)didAcceptConnectionForServer:(TCPServer *)server inputStream:(NSInputStream *)istr outputStream:(NSOutputStream *)ostr
{
    LOG(@"%s  _inStream:%d  _outStream:%d", (char*)_cmd, _inStream?true:false, _outStream?true:false);
    
    
    
    
	if (_inStream && _outStream && server != _server)
		return;
    
	_didAcceptConnection = YES;
    
    [_server release];
	_server = nil;
	
	_inStream = istr;
	[_inStream retain];
	_outStream = ostr;
	[_outStream retain];
	LOG(@"openStreams is called in didAcceptConnectionForServer");
    
    
    
	[self openStreams];
}



#pragma mark - NetworkBrowser Delegate
-(void)didFindNewService:(NetworkBrowser *)browserObj
{
    LOG(@"%s", (char*)_cmd);
    [self.delegate didFindNewService:browserObj.currentNetService.name];
    //[browserObj resolveServiceWithName:[browserObj.currentNetService name]];
}

-(void)didResolveService:(NetworkBrowser *)browserObj
{
    LOG(@"%s", (char*)_cmd);
    LOG(@"resolved service name? %@", [browserObj.currentNetService name]);
    
    // note the following method returns _inStream and _outStream with a retain count that the caller must eventually release
	if (![browserObj.currentNetService getInputStream:&_inStream outputStream:&_outStream]) {
		//[self _showAlert:@"Failed connecting to server"];
        LOG(@"Failed connecting to server");
		return;
	}
    
    LOG(@"openStreams is called in didResolveService");
	[self openStreams];
    
    
}

-(void)didRemoveConnectedService:(NSString *)serviceName
{
    self.isServiceAvailable = NO;
    LOG(@"%s", (char*)_cmd);
}
#pragma mark - NSStream handler

-(void)checkWaitingList
{
    if([_dictionaryArray count] > 0)
    {
        NSDictionary *dictListDict  = [NSDictionary dictionaryWithObject:_dictionaryArray forKey:DictionaryListKey];
        [self sendDictionary:dictListDict];
        [_dictionaryArray removeAllObjects];
    }
}



- (void) stream:(NSStream *)stream handleEvent:(NSStreamEvent)eventCode
{
    LOG(@"%s", (char*)_cmd);
	//UIAlertView *alertView;
	switch((int)eventCode) {
		case NSStreamEventOpenCompleted:
		{
            LOG(@"NSStreamEventOpenCompleted");
            
			[_server release];
			_server = nil;
            
			if (stream == _inStream)
				_inReady = YES;
			else
				_outReady = YES;
			
			if (_inReady && _outReady){
                _isReady = YES;
                [self.delegate didLCGetReady:self];
			}
			break;
		}
		case NSStreamEventHasBytesAvailable:
		{
            if(!_receivedData || _receivedData == NULL)
            {
                _receivedData = [[NSMutableData data] retain];
            }
            
            uint8_t buf[1024];
            unsigned int len = 0;
            len = [(NSInputStream *)stream read:buf maxLength:1024];
            if(len)
            {
                [_receivedData appendBytes:(const void *)buf length:len];
                
                if(len < 1024)
                {
                    @try
                    {
                        
                        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:_receivedData];
                        
                        NSDictionary *receivedDictionary =[[unarchiver decodeObjectForKey:EncodingKey] retain];
                        //[unarchiver decodeObjectForKey:EncodingKey];//[[unarchiver decodeObjectForKey:EncodingKey] retain];
                        [unarchiver finishDecoding];
                        [unarchiver release];

                        if(receivedDictionary)
                        {
                            [self.delegate didReceiveDictionary:receivedDictionary];
                            NSMutableArray * dictArray = [receivedDictionary valueForKey:DictionaryListKey];
                            if(dictArray)
                            {
                                for(int i =0; i< [dictArray count]; i++)
                                {
                                    [self.delegate didReceiveDictionary:[dictArray objectAtIndex:i]];
                                }
                            }
                            [receivedDictionary release];
                        }
                        [_receivedData setData:[NSData dataWithBytes:NULL length:0]];
                        [_receivedData release];
                        _receivedData = NULL;

                    }
                    @catch(NSException* ex)
                    {
                        [_receivedData setData:[NSData dataWithBytes:NULL length:0]];
                        [_receivedData release];
                        _receivedData = NULL;
                    }
                }
            } else {
                NSLog(@"no buffer!");
            }
			break;
		}
		case NSStreamEventErrorOccurred:
		{
			break;
		}
			
		case NSStreamEventEndEncountered:
		{
			break;
		}
        case NSStreamEventHasSpaceAvailable:
        {
            _outReady = YES;
        }
        break;
            
	}
}



@end
