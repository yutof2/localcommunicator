//
//  LocalCommunicator.h
//  SharedBoard
//
//  Created by Yuto Fujikawa on 5/11/13.
//  Copyright (c) 2013 Yuto Fujikawa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCPServer.h"
#import "NetworkBrowser.h"

#define ServiceName @"MySharedBoard"
#define EncodingKey @"JustInCase"
#define DictionaryListKey @"LC_DictionaryList"

@protocol LocalCommunicatorDelegate;

@interface LocalCommunicator : NSObject<TCPServerDelegate, NSStreamDelegate, NetworkBrowserDelegate>
{
    NetworkBrowser *_networkBrowser;
    NSMutableData *_receivedData;
    //Networking variables
    TCPServer			*_server;
    NSInputStream		*_inStream;
	NSOutputStream		*_outStream;
	BOOL				_inReady;
	BOOL				_outReady;
   // NSMutableArray *_waitingList;
    NSTimer *_waitingListChecker;
    NSMutableArray *_dictionaryArray;
    BOOL _didAcceptConnection;
    
}

@property(readonly) BOOL isReady;
@property(nonatomic, assign) NSDictionary *receivedDictionary;
@property (assign, nonatomic) id<LocalCommunicatorDelegate>delegate;
@property(assign, readwrite)BOOL isServiceAvailable;


-(void)sendDictionary:(NSDictionary*)dict;
-(void)addToDictionaryArray:(NSDictionary*)dict;
-(void)resolveService:(NSString*)serviceName;
//-(void)checkWaitingList;
@end


@protocol LocalCommunicatorDelegate <NSObject>
@optional
-(void)didLCGetReady:(LocalCommunicator*)localCommunicator;
-(void)didReceiveDictionary:(NSDictionary*)dict;
-(void)didFindNewService:(NSString*)serviceName;
-(void)didRemoveConnectedService:(NSString*)serviceName;

//-(void)didResolveService:(NetworkBrowser*)browserObj;
@end

