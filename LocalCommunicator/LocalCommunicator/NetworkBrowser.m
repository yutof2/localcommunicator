//
//  NetworkBrowser.m
//  SharedBoard
//
//  Created by Yuto Fujikawa on 4/28/13.
//  Copyright (c) 2013 Yuto Fujikawa. All rights reserved.
//

#import "NetworkBrowser.h"

@implementation NetworkBrowser
@synthesize serviceArray=_serviceArray;
@synthesize myNetService =_myNetService;
@synthesize netServiceBrowser=_netServiceBrowser;
@synthesize myServiceName =_myServiceName;
@synthesize delegate = _delegate;

-(id)initWithMyServiceName:(NSString*)myServicename_
{
    if(self =[super init])
     {
        self.serviceArray = [[[NSMutableArray alloc]init] autorelease];
        self.myServiceName = [myServicename_ copy];
    }
    
    return self;
}

-(void)dealloc
{
    [_serviceArray release];
    _serviceArray = nil;
    
    [_netServiceBrowser release];
    _netServiceBrowser = nil;
    
    [_myServiceName release];
    _myServiceName = nil;
    
    [_myNetService release];
    _myNetService = nil;
    
    [super dealloc];
}

-(void)searchForServicesOfType:(NSString*)typeString inDomain:(NSString *)domainString
{
	[self.netServiceBrowser stop];
	[self.serviceArray removeAllObjects];
    
	NSNetServiceBrowser *aNetServiceBrowser = [[NSNetServiceBrowser alloc] init];
	aNetServiceBrowser.delegate = self;
	self.netServiceBrowser = aNetServiceBrowser;
	[aNetServiceBrowser release];
	[self.netServiceBrowser searchForServicesOfType:typeString inDomain:domainString];

}


-(BOOL)resolveServiceWithName:(NSString*)serviceName
{
    LOG(@"%s", (char*)_cmd);
    LOG(@"for the service name %@", serviceName);
    
    
    NSNetService *serviceToBeResolved= nil;
    NSNetService *currentService= nil;
    int arrayLength = [self.serviceArray count];
    
    for(int i =0; i< arrayLength; i++)
    {
        currentService = [self.serviceArray objectAtIndex:i];
        LOG(@"service name? %@", [currentService name]);
        
        if([[currentService name] isEqualToString:serviceName])
        {
            serviceToBeResolved = currentService;
        }
    }
    
    if(serviceToBeResolved)
    {
        self.currentNetService = serviceToBeResolved;
        self.currentNetService.delegate = self;
        [self.currentNetService resolveWithTimeout:0.0];
    }
    
    //return false if the requested service was not found
    return (serviceToBeResolved != nil? YES : NO);
}


-(void)stopCurrentResolve
{
    //[self.currentNetService stop];
    //self.currentNetService = nil;
    
}

#pragma mark - NSNetServiceBrowser Delegate

- (void)netServiceBrowser:(NSNetServiceBrowser *)netServiceBrowser didFindService:(NSNetService *)netService moreComing:(BOOL)moreServicesComing
{
    LOG(@"%s",(char*)_cmd);
    
    if([[netService name] isEqualToString:self.myServiceName])
    {
        LOG(@"my service found!");
        self.myNetService = netService;
    }
    else
    {
        LOG(@"find a new service of a name? %@   more coming? %d", [netService name], moreServicesComing);
        [self.serviceArray addObject:netService];
        
        LOG(@"service length? %d", self.serviceArray.count);
        
        self.currentNetService = netService;
        [self.delegate didFindNewService:self];
    }
}


- (void)netServiceBrowser:(NSNetServiceBrowser *)netServiceBrowser didRemoveService:(NSNetService *)service moreComing:(BOOL)moreComing
{
    
    
    //(@"netServiceBrowser:didRemoveService of service name %@ current service:%@ ",[service name],self.currentNetService.name);
    
	// If a service went away, stop resolving it if it's currently being resolved,
	// remove it from the list and update the table view if no more events are queued.
	[self.serviceArray removeObject:service];
    
    LOG(@"service count? %d", [self.serviceArray count]);
    //[self.delegate didRemoveConnectedService:[service name]];
    
    
    if([service.name isEqualToString:self.currentNetService.name] == YES)
    {
        [self.delegate didRemoveConnectedService:[service name]];
    }
    
	if (self.myNetService == service)
		self.myNetService = nil;

    
	
	// If moreComing is NO, it means that there are no more messages in the queue from the Bonjour daemon, so we should update the UI.
	// When moreComing is set, we don't update the UI so that it doesn't 'flash'.
}



#pragma mark - NSNetService Delegate

- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary *)errorDict {
    LOG(@"%s", (char*)_cmd);
    
	[self stopCurrentResolve];
	//[self.tableView reloadData];
}

- (void)netServiceDidResolveAddress:(NSNetService *)service {
	//assert(service == self.currentResolve);
    LOG(@"%s", (char*)_cmd);
	
	[service retain];
	//[self stopCurrentResolve];
	self.currentNetService = service;
    [self.delegate didResolveService:self];
	//[self.delegate browserViewController:self didResolveInstance:service];
	[service release];
}


@end
