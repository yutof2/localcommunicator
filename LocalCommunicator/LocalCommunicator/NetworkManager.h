//
//  NetworkManager.h
//  SharedBoard
//
//  Created by Yuto Fujikawa on 6/2/13.
//  Copyright (c) 2013 Yuto Fujikawa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCPServer.h"
#import "NetworkBrowser.h"
#import "LocalCommunicator.h"
@interface NetworkManager : NSObject<TCPServerDelegate, NetworkBrowserDelegate>
{
    NetworkBrowser *_networkBrowser;
    TCPServer			*_server;
    NSMutableArray *_communicatorArray;
}
@end
