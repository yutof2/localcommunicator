//
//  NetworkManager.m
//  SharedBoard
//
//  Created by Yuto Fujikawa on 6/2/13.
//  Copyright (c) 2013 Yuto Fujikawa. All rights reserved.
//

#import "NetworkManager.h"
@interface NetworkManager(private)
-(void)setupServerBrwoserWithName:(NSString*)serviceName;
@end


@implementation NetworkManager

-(void)dealloc
{
    [_server release];
    _server = nil;
    [_communicatorArray release];
    [_networkBrowser release];
    
    [super dealloc];
    
}

-(void)setupServer
{
    LOG(@"%s",(char*)_cmd);

	_server = [TCPServer new];
	[_server setDelegate:self];
	NSError *error = nil;
	if(_server == nil || ![_server start:&error]) {
		if (error == nil) {
			LOG(@"Failed creating server: Server instance is nil");
		} else {
            LOG(@"Failed creating server: %@", error);
		}
		return;
	}
	
	//Start advertising to clients, passing nil for the name to tell Bonjour to pick use default name
	if(![_server enableBonjourWithDomain:@"local" applicationProtocol:[TCPServer bonjourTypeFromIdentifier:ServiceName] name:nil]) {
		LOG(@"Failed advertising server");
		return;
	}
    
    _communicatorArray = [[NSMutableArray alloc]init];
    
}


#pragma mark - TCPServerDelegate
-(void)setupServerBrwoserWithName:(NSString*)serviceName
{
    LOG(@"%s",(char*)_cmd);
    LOG(@"service name %@", serviceName);
    _networkBrowser = [[NetworkBrowser alloc]initWithMyServiceName:serviceName] ;
    _networkBrowser.delegate = self;
    [_networkBrowser searchForServicesOfType:[TCPServer bonjourTypeFromIdentifier:ServiceName] inDomain:@"local"];
}

- (void) serverDidEnableBonjour:(TCPServer *)server withName:(NSString *)string
{
	LOG(@"%s", (char*)_cmd);
	//[self presentPicker:string];
    //[self setUpServerBrwoser];
    [self setupServerBrwoserWithName:string];
    
}

- (void)didAcceptConnectionForServer:(TCPServer *)server inputStream:(NSInputStream *)istr outputStream:(NSOutputStream *)ostr
{
    /*
	if (_inStream || _outStream || server != _server)
		return;
	
	[_server release];
	_server = nil;
	
	_inStream = istr;
	[_inStream retain];
	_outStream = ostr;
	[_outStream retain];
	
	[self openStreams];
    */
    
}

#pragma mark - NetworkBrowser Delegate
-(void)didFindNewService:(NetworkBrowser *)browserObj
{
    LOG(@"%s", (char*)_cmd);
    [browserObj resolveServiceWithName:[browserObj.currentNetService name]];
}

-(void)didResolveService:(NetworkBrowser *)browserObj
{
    LOG(@"%s", (char*)_cmd);
    LOG(@"resolved service name? %@", [browserObj.currentNetService name]);
    
    // note the following method returns _inStream and _outStream with a retain count that the caller must eventually release
	/*
    if (![browserObj.currentNetService getInputStream:&_inStream outputStream:&_outStream]) {
		//[self _showAlert:@"Failed connecting to server"];
        LOG(@"Failed connecting to server");
		return;
	}
    */
    
	//[self openStreams];
    
    
}

@end
